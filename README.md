# Rollbot
A locally-running Roll20 "API" that sends messages via HTTP. It currently does not run headless (as logging in is done manually), and only sends messages (not receiving them), but it's primary purpose is triggering actions in a Roll20 script (which don't have outside communication) from a C++ application, and it should be able to do that successfully.
